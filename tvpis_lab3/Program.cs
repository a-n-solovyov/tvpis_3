﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace tvpis_lab3
{
    //SELECT a, b+2 * 3  FROM bla EXCEPT SELECT a FROM qq UNION ALL SELECT 3 + dd * 4 FROM qq INTERSECT SELECT a FROM b UNION SELECT d FROM c

    static class TextWriterExtensions
    {
        public static void WriteIndent(this TextWriter o, int depth)
        {
            o.Write(new string(' ', depth * 2));
        }
        
        public static void WriteIndentLine(this TextWriter o, int depth)
        {
            o.Write(o.NewLine + new string(' ', depth * 2));
        }
    }

    interface INode
    {
        string ToFormattedString();
        void DebugPrint(TextWriter o, int depth);
    }

    interface IExpression : INode
    {
    }

    interface IBinaryExpression : IExpression
    {
        IExpression Left { get; }

        BinaryOperatorType Operator { get; }

        IExpression Right { get; }
    }

    interface ISelectStmt : INode
    {
    }

    internal enum BinaryOperatorType
    {
        Sum,
        Product
    }

    internal enum CompoundOperator
    {
        Union,
        UnionAll,
        Except,
        Intersect
    }

    sealed class Identifier : IExpression
    {
        public string Lexeme { get; }

        public string ToFormattedString() => Lexeme;

        public void DebugPrint(TextWriter o, int depth)
        {
            o.WriteIndentLine(depth);
            o.Write("new Identifier( ");
            o.Write($"\"{Lexeme}\" ");
            o.Write(")");
        }

        public Identifier(string lexeme)
        {
            Lexeme = lexeme;
        }
    }


    sealed class Number : IExpression
    {
        public string Value { get; }

        public Number(string value)
        {
            Value = value;
        }

        public string ToFormattedString() => Value;

        public void DebugPrint(TextWriter o, int depth)
        {
            o.WriteIndentLine(depth);
            o.Write("new Number( ");
            o.Write($"\"{Value}\" ");
            o.Write(")");
        }
    }
    
    sealed class BinaryExpression : IBinaryExpression
    {
        public BinaryExpression(IExpression left, BinaryOperatorType op, IExpression right)
        {
            Left = left;
            Operator = op;
            Right = right;
        }

        public string ToFormattedString()
        {
            return $"{Left.ToFormattedString()} {OperatorToString()} {Right.ToFormattedString()}";
        }

        public void DebugPrint(TextWriter o, int depth)
        {
            o.WriteIndentLine(depth);
            o.Write("new BinaryExpression(");
            Left.DebugPrint(o, depth+1);
            o.WriteIndentLine(depth+1);
            o.Write($"{OperatorToString()}, ");
            Right.DebugPrint(o, depth+1);
            o.Write(")");
        }
        
        private string OperatorToString()
        {
            switch (Operator)
            {
                case BinaryOperatorType.Product:
                    return "BinaryOperatorType.Product";
                case BinaryOperatorType.Sum:
                    return "BinaryOperatorType.Sum";
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public IExpression Left { get; }

        public BinaryOperatorType Operator { get; }
        
        public IExpression Right { get; }
    }
    

    sealed class Table : INode
    {
        public string Name { get; }

        public string ToFormattedString() => Name;

        public void DebugPrint(TextWriter o, int depth)
        {
            o.WriteIndentLine(depth);
            o.Write("new Number( ");
            o.Write($"\"{Name}\" ");
            o.Write(")");
        }

        public Table(string name)
        {
            Name = name;
        }
    }

    sealed class SelectStmt : ISelectStmt
    {
        public IReadOnlyList<IExpression> ResultColumns { get; }

        public Table From { get; }

        public SelectStmt(Table from, params IExpression[] resultColumns)
        {
            if (resultColumns.Length == 0) throw new ArgumentException(nameof(resultColumns));
            ResultColumns = resultColumns;
            From = from;
        }

        public string ToFormattedStringColumns()
        {
            return string.Join(", ", ResultColumns);
        }

        public void DebugPrintColumns(TextWriter o, int depth)
        {
            foreach (IExpression expression in ResultColumns)
            {
                expression.DebugPrint(o, depth + 1);
                o.Write(", ");
            }
        }

        public string ToFormattedString() => $"SELECT {ToFormattedStringColumns()} FROM {From.ToFormattedString()}";

        public void DebugPrint(TextWriter o, int depth)
        {
            o.WriteIndentLine(depth);
            o.Write("new SelectStmt(");
            o.WriteIndent(depth + 1);
            DebugPrintColumns(o, depth + 1);
            From.DebugPrint(o, depth + 1);
            o.WriteIndentLine(depth+1);
            o.Write(")");
        }
    }

    sealed class CompoundSelect : ISelectStmt
    {
        public ISelectStmt Left { get; }

        public CompoundOperator Operator { get; }

        public ISelectStmt Right { get; }

        public CompoundSelect(ISelectStmt left, CompoundOperator op, ISelectStmt right)
        {
            Left = left;
            Operator = op;
            Right = right;
        }

        public string ToFormattedString()
        {
            return $"{Left.ToFormattedString()}\n{OperatorToString()}\n{Right.ToFormattedString()}";
        }

        public void DebugPrint(TextWriter o, int depth)
        {
            o.WriteIndentLine(depth);
            o.Write("new CompoundSelect(");
            o.WriteIndent(depth + 1);
            Left.DebugPrint(o, depth + 1);
            o.WriteIndentLine(depth+1);
            Right.DebugPrint(o, depth+1);
            o.WriteIndentLine(depth+1);
            o.Write(")");
        }

        private string OperatorToString()
        {
            switch (Operator)
            {
                case CompoundOperator.Union:
                    return "CompoundOperator.Union";
                case CompoundOperator.UnionAll:
                    return "CompoundOperator.UnionAll";
                case CompoundOperator.Except:
                    return "CompoundOperator.Except";
                case CompoundOperator.Intersect:
                    return "CompoundOperator.Intersect";
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }

    static class NodeExtensions
    {
        public static string ToDebugString(this INode node)
        {
            using (var o = new StringWriter())
            {
                node.DebugPrint(o, 0);
                o.Write("\n");
                return o.ToString();
            }
        }
    }


    internal class Program
    {
        //     SELECT a, b+2 * 3  FROM bla 
        // EXCEPT 
        //     SELECT a FROM qq 
        // UNION ALL 
        //     SELECT 3 + dd * 4 FROM qq 
        // INTERSECT 
        //     SELECT a FROM b 
        // UNION 
        //     SELECT d FROM c
        public static void Main(string[] args)
        {
            var tree = new CompoundSelect(new SelectStmt(
                    new Table("bla"),
                    new IExpression[]
                    {
                        new Identifier("a"),
                        new BinaryExpression(
                            new Identifier("b"),
                            BinaryOperatorType.Sum,
                            new BinaryExpression(
                                new Number("2"),
                                BinaryOperatorType.Product,
                                new Number("3")
                            )
                        ), 
                    }
                ),
                CompoundOperator.Except,
                new CompoundSelect(
                    new SelectStmt(new Table("qq"), new Identifier("a")),
                    CompoundOperator.UnionAll,
                    new CompoundSelect(
                        new SelectStmt(
                                new Table("qq"),
                                new BinaryExpression(
                                    new Number("3"),
                                    BinaryOperatorType.Sum,
                                    new BinaryExpression(
                                        new Identifier("dd"),
                                        BinaryOperatorType.Product,
                                        new Number("4")
                                    )
                                )
                        ), 
                        CompoundOperator.Intersect,
                        new CompoundSelect(
                                new SelectStmt(new Table("a"), new Identifier("b")),
                                CompoundOperator.Union,
                                new SelectStmt(new Table("c"), new Identifier("d"))
                        )
                    )
                )
            );
            Console.WriteLine(tree.ToDebugString());
        }
    }
}